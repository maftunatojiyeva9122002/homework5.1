import React from "react";
export default class App extends React.Component {
  state = {
    show: true,
  };

  add = () => {
    this.setState((state) => (state.show = true));
  };
  hide = () => {
    this.setState((state) => (state.show = false));
  };

  render() {
    return (
      <div
        style={{
          border: "2px solid black",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          width: "500px",
          padding: "50px",
          borderRadius: "15px",
        }}
      >
        <div style={{ display: "flex", gap: "50px" }}>
          {this.state.show ? (
            <div
              style={{
                display: "flex",
                gap: "20px",
                alignItems: "center",
                border: "2px solid black",
                padding: "10px 10px",
              }}
            >
              <h1
                style={{
                  borderBottom: "1px solid black",
                  paddingBottom: "5px",
                }}
              >
                My component
              </h1>
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                gap: "20px",
                alignItems: "center",
                border: "2px solid red",
                padding: "10px 10px",
              }}
            >
              <h1 style={{ color: "red" }}>Nothing...</h1>
            </div>
          )}

          <div
            style={{ display: "flex", flexDirection: "column", gap: "20px" }}
          >
            <button
              onClick={this.add}
              style={{
                border: "2px solid black",
                background: "transparent",
                padding: "8px 20px",
                borderRadius: "5px",
              }}
            >
              show
            </button>
            <button
              onClick={this.hide}
              style={{
                border: "2px solid black",
                background: "transparent",
                padding: "8px 20px",
                borderRadius: "5px",
              }}
            >
              hide
            </button>
          </div>
        </div>
      </div>
    );
  }
}
